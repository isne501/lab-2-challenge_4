#include <iostream>
#include <string>
#include "tree.h"
#include <sstream> //include stringstream to saperate each word form line
#include <fstream> //include filestream to open the file .txt
#include <vector>
using namespace std;
void main()
{
	Tree<string> mytree; //Create the tree
	vector<string> wordline; //Create vector to store each word line
	string word,subword; //To store saperate word and sup-word
	
	//Add file text to this program
	ifstream file; 
	file.open("word.txt");
	
	int line = 1; //Start line with line number 1
	
	while (getline(file, word)) //loop for get line in text file to saperate the word line and stop when doesn't have line anymore
	{
		wordline.push_back(word); //Push each word line in to the vector
		stringstream sup(word);
		while (getline(sup,subword, ' '))//pass stringstream , variable to store each word , Blank space (for getline to saperate the word ) 
		{
			subword = subword+ " " + to_string(line)+"\n"; //sub-word and the number of line
			mytree.insert(subword); //Add subword to the tree
		}
		line++; //Change to next line
	}
		
		mytree.inorder(); //See every node by LVR (left visit right) and output them
		
		int number_line;
		//Told the user input word line number to see whole word
		cout << "Input word line number(s) for see the whole words : ";
		cin >> number_line; 
		cout << wordline.at(number_line - 1) << endl; //Show the word and (number_line - 1) because vector start at index 0
	system("PAUSE");
}
